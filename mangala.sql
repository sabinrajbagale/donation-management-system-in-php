-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2019 at 06:32 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mangala`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'sabin', 'sabin'),
(2, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `doner`
--

CREATE TABLE `doner` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `permaddr` varchar(100) NOT NULL,
  `tempaddr` varchar(100) NOT NULL,
  `amount` int(20) NOT NULL,
  `date` date NOT NULL,
  `contactno` varchar(10) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doner`
--

INSERT INTO `doner` (`id`, `name`, `permaddr`, `tempaddr`, `amount`, `date`, `contactno`, `fname`, `mname`) VALUES
(4, 'sdhgf', 'fgjgh', 'vbmkg', 2147483647, '0000-00-00', '1111111111', 'dsbgf', 'vmgj'),
(5, 'sabinrajaaw', 'hariharasuta', 'RDFHFG', 2019, '0000-00-00', '0741143751', 'sabinraj', 'DFJ'),
(6, 'sabinraj bagale', 'hariharasuta', 'dfhjgh', 345678, '0000-00-00', '0741143751', 'sabinraj', 'sdfgvbh'),
(7, 'ab', 'dfgh', 'asdfghgj', 345678, '2019-04-10', '345678', 'sabinraj', 'saraswotib'),
(8, 'wret', 'sdgf', 'asgs', 34567, '0000-00-00', '345678', 'asdfg', 'sdfggh'),
(10, 'sabinraj bagale', 'hariharasuta', 'ui', 2, '0000-00-00', '0741143751', 'sabinraj', 'tty'),
(13, 'sabin', 'suryapal-7,lamjung', 'hariharasuta,lamjung', 0, '0000-00-00', '0741143751', 'sabinraj', 'saraswotibjhashkfjsdfj'),
(15, 'asdf', 'asdf', 'ffff', 345678, '2020-04-06', '0741143751', 'sabinraj', 'tttt'),
(16, 'rosan', 'jnk', 'jnk', 1999, '2019-04-24', '34567', 'sdfgh', 'waert');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doner`
--
ALTER TABLE `doner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `doner`
--
ALTER TABLE `doner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
