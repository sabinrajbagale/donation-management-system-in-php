<?php
session_start();
if(isset($_SESSION['uid']))
{
	echo "";
}
else
{
	header('location: ../index.php');
}
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<style>
			h1{
			font-size: 50px;
			color: red;
			text-shadow: 5px 8px 12px blue;
			text-align: center;
			top: 1%
			}
				.row{
					margin-top: 20px;
					padding: 60px;
				}
				
				post{
				width: 500px;
				height: 350px;
				position: relative;
				cursor: pointer;
				}
				.post:hover .post-s{
				height: 300px;
				width: 93%;
				}
				.post img {
				display: block;
				width: 100%;
				height: 300px;
				}
				.post-s{
				width: 50%;
				height: 300px;
				background: rgba(103, 58, 183, 0.71);
				position: absolute;
				top:1px;
				display: flex;
				justify-content: center;
				align-items: center;
				overflow: hidden;
				transition: 0.7s ease;
				}
				.post-s h2{
				color: white;
				font-size: 30px;
				border: 6px solid white;
				padding: 10px 10px;
				}
		</style>
	</head>
	<body background="../img/bgadmin.jpg" style="background-repeat: no-repeat; background-size: cover" >
		<a href="logout.php" style="float:right; margin-right: 20px;"><img src="../img/logout.png" alt="logout" height="80px" width="130px"></a>
		<br>
		<br>
		<h1><b>WELCOME TO ADMIN DASHBOARD</b></h1>
		<a href="viewall.php" style="float:right; margin-right: 20px;"><img src="../img/view.png" alt="viewall" height="70px" width="100px"></a>
		<section id="education">
			<div class="container-fluid">
				<div class="row edu">
					<div class="col-md-4 col-sm-12 col-xs-12 col-12 p-2" >
						<div class="post">
							<a href="addstudent.php">
								<img class="card-img-top"src="../img/add.jpg"  alt="">
								
								<div class="post-s">
									<h2>ADD <br>DONATION DETAILS</h2>
								</div>
							</a>
						</div>
					</div>
					<div class=" col-md-4 col-sm-12 col-xs-12 col-12 p-2">
						<div class="post">
							<a href="updatestudent.php">
								<img class="card-img-top"src="../img/update.jpg"   alt="">
								<div class="post-s">
									<h2>UPDATE <br>DONATION DETAILS</h2>
								</div>
							</a>
						</div>
						
					</div>
					
					<div class="col-md-4 col-sm-12 col-xs-12 col-12 p-2">
						
						<div class="post">
							<a href="deletestudent.php">
								<img class="card-img-top"src="../img/delete.jpg"   alt="">
								<div class="post-s">
									<h2>DELETE <br>DONATION DETAILS<br></h2>
								</div>
							</a>
						</div>
						
					</div>
				</div>
			</div>
		</section>
	</body>
</html>