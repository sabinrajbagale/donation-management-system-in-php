<?php
// Ascending Order
if(isset($_POST['ASC']))
{
$asc_qry = "SELECT * FROM doner ORDER BY amount ASC";
$run = executeQuery($asc_qry);
}
// Descending Order
elseif (isset ($_POST['DESC']))
{
$desc_qry = "SELECT * FROM doner ORDER BY amount DESC";
$run = executeQuery($desc_qry);
}
// Default Order
else {
$default_qry = "SELECT * FROM doner";
$run = executeQuery($default_qry);
}
function executeQuery($qry)
{
$con = mysqli_connect('localhost','root','','mangala');
$run=mysqli_query($con,$qry);
return $run;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title> Total List</title>
        <style>
        table,tr,th,td
        {
        
        text-align: left;
        }
         #HTMLtoPDF a{
        float: right;
      }
        </style>
    </head>
    <body>
        
         <a href="viewall.php"><h2 align="center">MANGALA SECONDARY SCHOOL</h2></a>
        <form action="order.php" method="post">
            
            <input type="submit" name="ASC" value="asc">
            <input type="submit" name="DESC" value="des">
          <div id="HTMLtoPDF">
            <!-- here we call the function that makes PDF -->
            <a href="#" onclick="HTMLtoPDF()">Download PDF</a>
            <button onclick="mywholepage()">print</button><br><br><br>
            <script type="text/javascript">
            function mywholepage(){
            window.print();
            }
            </script>
            <table align="center" width="85%" >
                <tr>
                    <th>NO. </th>
                    <th>Name </th>
                    <th>Perm Addr </th>
                    <th>Amount </th>
                    <th>Date</th>
                    <th>Contact No. </th>
                    <th>Father's Name </th>
                </tr>
                <!-- populate table from mysql database -->
                <?php
                $count=0;
                while($data=mysqli_fetch_array($run))
                {
                $count++;
                ?>
                
                <tr align="center">
                    <td><?php echo $count;   ?></td>
                    <td><?php echo $data['name']; ?></td>
                    <td><?php echo $data['permaddr']; ?></td>
                    <td><?php echo $data['amount'];   ?></td>
                    <td><?php echo $data['date'];   ?></td>
                    <td><?php echo $data['contactno'];?></td>
                    <td><?php echo $data['fname']; ?></td>
                    
                </tr>
                <?php
                }
                ?>
            </table>
        </form>
        
        <!-- these js files are used for making PDF -->
        <script src="../js/jspdf.js"></script>
        <script src="../js/jquery-2.1.3.js"></script>
        <script src="../js/pdfFromHTML.js"></script>
    </div>
    </body>
</html>