<?php
session_start();
if(isset($_SESSION['uid']))
{
header('location: admin/admindash.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>login</title>
        <style>
        
        body{
        font-family: sans-serif;
        }
        h1{
        font-size: 50px;
        color: red;
        text-shadow: 5px 8px 12px green;
        text-align: center;
        top: 20%
        }
        .login-box{
        width: 310px;
        height: 360px;
        background: rgba(225, 225, 0, 0.6);
        color:blue;
        top: 70%;
        left: 50%;
        position: absolute;
        transform: translate(-50%,-50%);
        box-sizing: border-box;
        padding: 29px 40px;
        }
        .mangala{
        width: 80px;
        height: 80px;
        border-radius: 50%;
        position: absolute;
        top: -50px;
        left: calc(50% - 50px);
        }
        h2{
        margin: 0;
        padding: 0 0 20px;
        text-align: center;
        font-size: 22px;
        }
        .login-box p{
        margin: 0;
        padding: 0;
        font-weight: bold;
        font-size: 30px
        }
        .login-box input{
        width: 100%;
        margin-bottom: 20px;
        font-size: 20px
        }
        .login-box input[type="text"], input[type="password"]
        {
        border: none;
        border-bottom: 1px solid pink;
        background: transparent;
        outline: none;
        height: 40px;
        color: red;
        font-size: 20px;
        }
        .login-box input[type="submit"]
        {
        border: none;
        outline: none;
        height: 40px;
        background: blue;
        color: red;
        font-size: 20px;
        border-radius: 20px;
        }
        .login-box input[type="submit"]:hover
        {
        cursor: pointer;
        background: grey;
        color: pink;
        }
        img{
        opacity: 1;
        }
        
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12 col-12 p-2" >
                    <body background="img/b.jpg" style="background-repeat: no-repeat; background-size:cover" >
                        <a href="index.php" style="float:left;">
                            <img src="img/home.png" alt="Home" height="70px" width="120px">
                        </a>
                        <h1>MANGALA SECONDARY SCHOOL</h1>
                        <div class="login-box">
                            <img src="img/a.jpg" class="mangala">
                            <br>
                            <h2>Admin login</h2>
                            <form action="login.php" method="post">
                                <p>Username</p>
                                <input type="text" name="username" placeholder="Enter Username" required>
                                <p>Password</p>
                                <input type="password" name="password" placeholder="Enter Password" required>
                                <br>
                                <br>
                                <input type="submit"  name="login" value="login">
                                
                            </form>
                        </div>
                        
                    </div>
                    
                </div>
                
            </body>
        </html>
        <?php
        include('dbcon.php');
        if(isset($_POST['login']))
        {
        $username=$_POST['username'];
        $password=$_POST['password'];
        $qry="SELECT * FROM `admin` WHERE `username`='$username' AND `password`='$password'";
        $run=mysqli_query($con,$qry);
        $row=mysqli_num_rows($run);
        if($row<1)
        {
        ?>
        <script>
        alert('username and password not matched');
        window.open('login.php','_self');
        </script>
        <?php
        }
        else
        {
        $data=mysqli_fetch_assoc($run);
        $id=$data['id'];
        echo " id=".$id;
        session_start();
        $_SESSION['uid']=$id;
        header('location:admin/admindash.php');
        }
        }
        ?>