<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<style>
			table th,td{
				text-align: left;
				border: 1 px;
				line-height: 27px;
				padding: 5px;
				margin: 0px;
			}
			#HTMLtoPDF a{
				float: right;
			}
		</style>
	</head>
	<body>
		<a href="index.php"><h2 align="center">MANGALA SECONDARY SCHOOL</h2></a>
		<div id="HTMLtoPDF">
			<!-- here we call the function that makes PDF -->
			<a href="#" onclick="HTMLtoPDF()">Download PDF</a>
			<button onclick="mywholepage()"  >print</button>
			<script type="text/javascript">
				function mywholepage(){
					window.print();
				}
			</script>
			<table align="center" width="85%" >
				
				<tr>
					<th>NO.</th>
					<th>Name</th>
					<th>Perm Addr</th>
					<th>Amount</th>
					<th>Date</th>
					<th>Contact Num</th>
					<th>Father N</th>
				</tr>
				<br>
				<?php
				include('dbcon.php');
				
				$qry="SELECT * FROM `doner` ";
				$run=mysqli_query($con,$qry);
				?>
				
				<?php
				$count=0;
				while($data=mysqli_fetch_array($run))
				{
				$count++;
				?>
				<tr align="center">
					<td><?php echo $count;   ?></td>
					<td><?php echo $data['name']; ?></td>
					<td><?php echo $data['permaddr']; ?></td>
					<td><?php echo $data['amount'];   ?></td>
					<td><?php echo $data['date'];   ?></td>
					<td><?php echo $data['contactno'];?></td>
					<td><?php echo $data['fname']; ?></td>
				</tr>
				
				
				<?php
				}
				?>
				<!-- these js files are used for making PDF -->
				<script src="js/jspdf.js"></script>
				<script src="js/jquery-2.1.3.js"></script>
				<script src="js/pdfFromHTML.js"></script>
			</table>
		</div>
	</body>
</html>